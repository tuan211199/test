// Slick 

var btn = document.querySelector('.slick-prev');
var btnNext = document.querySelector('.slick-next');
$('.multiple-items').slick({
    infinite: true,
    arrows: true,
    slidesToShow: 4,
    slidesToScroll: 4,

});

// Scroll 
var menu = document.getElementById('menu');
var main = document.getElementById('main');
var origOffsetY = main.offsetTop;
var logoHead = document.getElementById('logoHead');

function onScroll(e) {
    if (window.scrollY >= origOffsetY) {
        menu.classList.add('menu2');
        menu.classList.remove('menu1');
        logoHead.setAttribute('src', 'image/2n.png');
    } else {
        menu.classList.add('menu1');
        menu.classList.remove('menu2');
        logoHead.setAttribute('src', 'image/0.png');
    }
}

document.addEventListener('scroll', onScroll);